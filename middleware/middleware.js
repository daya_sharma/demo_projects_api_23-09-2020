var express=require('express');
var app=express();      // create a common variable for express

//install npm bodyparser package that helps to get a request in json(JavaScript Object Notation) format
var bodyparser=require('body-parser');
// create a common routing file where all api routing path allocated  
var route=require('../route/route');

// call that puting file in other words use that route file 
app.use(route);

//define dependencies and modules  (npm ecosystem) 
module.exports=app
