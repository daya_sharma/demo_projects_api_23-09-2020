var express=require('express');
var app=express();      // create a common variable for express

// locate api file where each api is located 
var api=require('../controller/api');

// call the api function
app.get("/getCategory/:id?/:parent_id?",function(req,res){
    api.getCategory(req,res);
  });

  // ProductCategoryDeletion
  app.get("/ProductCategoryDeletion/:id?",function(req,res){
    api.ProductCategoryDeletion(req,res);
  });
  

//define dependencies and modules  (npm ecosystem) 
module.exports=app